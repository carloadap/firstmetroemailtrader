/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstmetro.emailreader;

import com.firstmetro.model.Recognia;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Vector;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import ndxlib.mail.EmailMessage;

/**
 *
 * @author Carlo
 */
public class DataObject {

    private Vector<ScheduledFuture<?>> ScheduleHandlers;
    private ScheduledExecutorService FScheduler;
    private Session session;
    private Folder folder;
    private String username;
    private String password;
    private String smtpHost;
    private String smtpServer;
    private Message[] messages;
    private String notification;
    private Store store;
    private Queue<Object> stack = new LinkedList<>();
    private List<EmailMessage> emailMessage = new LinkedList<>();
    private List<String> keyword = new LinkedList<>();
    private StringBuilder recipients;
    private List<Recognia> recognias = new LinkedList<>();

    /**
     * @return the ScheduleHandlers
     */
    public Vector<ScheduledFuture<?>> getScheduleHandlers() {
        return ScheduleHandlers;
    }

    /**
     * @param ScheduleHandlers the ScheduleHandlers to set
     */
    public void setScheduleHandlers(Vector<ScheduledFuture<?>> ScheduleHandlers) {
        this.ScheduleHandlers = ScheduleHandlers;
    }

    /**
     * @return the FScheduler
     */
    public ScheduledExecutorService getFScheduler() {
        return FScheduler;
    }

    /**
     * @param FScheduler the FScheduler to set
     */
    public void setFScheduler(ScheduledExecutorService FScheduler) {
        this.FScheduler = FScheduler;
    }

    /**
     * @return the session
     */
    public Session getSession() {
        return session;
    }

    /**
     * @param session the session to set
     */
    public void setSession(Session session) {
        this.session = session;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the folder
     */
    public Folder getFolder() {
        return folder;
    }

    /**
     * @param folder the folder to set
     */
    public void setFolder(Folder folder) {
        this.folder = folder;
    }

    /**
     * @return the messages
     */
    public Message[] getMessages() {
        return messages;
    }

    /**
     * @param messages the messages to set
     */
    public void setMessages(Message[] messages) {
        this.messages = messages;
    }

    /**
     * @return the notification
     */
    public String getNotification() {
        return notification;
    }

    /**
     * @param notification the notification to set
     */
    public void setNotification(String notification) {
        this.notification = notification;
    }

    /**
     * @return the store
     */
    public Store getStore() {
        return store;
    }

    /**
     * @param store the store to set
     */
    public void setStore(Store store) {
        this.store = store;
    }

    /**
     * @return the stack
     */
    public Queue<Object> getStack() {
        return stack;
    }

    /**
     * @return the smtpHost
     */
    public String getSmtpHost() {
        return smtpHost;
    }

    /**
     * @param smtpHost the smtpHost to set
     */
    public void setSmtpHost(String smtpHost) {
        this.smtpHost = smtpHost;
    }

    /**
     * @return the smtpServer
     */
    public String getSmtpServer() {
        return smtpServer;
    }

    /**
     * @param smtpServer the smtpServer to set
     */
    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    /**
     * @return the emailMessage
     */
    public List<EmailMessage> getEmailMessage() {
        return emailMessage;
    }

    /**
     * @return the recipients
     */
    public StringBuilder getRecipients() {
        return recipients;
    }

    /**
     * @param recipients the recipients to set
     */
    public void setRecipients(StringBuilder recipients) {
        this.recipients = recipients;
    }

    /**
     * @return the keyword
     */
    public List<String> getKeyword() {
        return keyword;
    }

    /**
     * @return the recognias
     */
    public List<Recognia> getRecognias() {
        return recognias;
    }
}
