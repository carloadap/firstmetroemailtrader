/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstmetro.emailreader;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlo
 */
public class FirstMetroEmailReader {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // TODO code application logic here
            
            EmailReaderService emailReaderService = EmailReaderService.getInstance();
            emailReaderService.start();

            System.in.read();
            System.exit(0);
        } catch (IOException ex) {
            Logger.getLogger(FirstMetroEmailReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
