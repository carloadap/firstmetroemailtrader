/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstmetro.emailreader;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.search.SearchTerm;

/**
 *
 * @author Carlo
 */
public class Search extends SearchTerm {

    String keyword;

    public Search(String keyword) {
        this.keyword = keyword;
    }

    @Override
    public boolean match(Message msg) {
        try {
            if (msg.getSubject().contains(keyword)) {
                return true;
            }
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
