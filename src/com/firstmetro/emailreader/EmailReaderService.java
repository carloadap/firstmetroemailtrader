/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstmetro.emailreader;

import com.firstmetro.state.StateManager;
import java.beans.PropertyChangeListener;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Stack;

/**
 *
 * @author Carlo
 */
public class EmailReaderService implements IService {

    ScheduledExecutorService fScheduler = Executors.newScheduledThreadPool(1);
    Vector<ScheduledFuture<?>> scheduleHandlers = new Vector<>();
    private Stack<PropertyChangeListener> stack = new Stack<>();
    private static EmailReaderService instance;

    public static EmailReaderService getInstance() {
        if (instance == null) {
            instance = new EmailReaderService();
        }
        return instance;
    }

    /**
     * Email Service
     */
    @Override
    public void start() {
        TaskFactory taskFactory = TaskFactory.getInstance();
        String staticTaskName = StateManager.class.getName();
        Runnable stateManager = taskFactory.createTaskComponent(staticTaskName, scheduleHandlers, fScheduler);
        fScheduler.scheduleWithFixedDelay(stateManager, 1, 20, TimeUnit.SECONDS);
        Logger.getLogger(EmailReaderService.class.getName()).log(Level.SEVERE, "Email Service Started");
    }
}
