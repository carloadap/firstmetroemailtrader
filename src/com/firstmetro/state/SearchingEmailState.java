/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstmetro.state;

import com.firstmetro.emailreader.DataObject;
import com.firstmetro.emailreader.Search;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Flags;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.search.FlagTerm;

/**
 *
 * @author Carlo
 */
public class SearchingEmailState implements IState {

    @Override
    public IState next(DataObject dataObject) {
        IState retval = null;
        try {
            Message foundMessages[] = dataObject.getFolder().search(new FlagTerm(
                    new Flags(Flags.Flag.SEEN), false));
            // performs search through the folder
            for (String keyword : dataObject.getKeyword()) {
                Search search = new Search(keyword);
                foundMessages = dataObject.getFolder().search(search, foundMessages);
            }
            if (foundMessages.length > 0) {
                dataObject.setMessages(foundMessages);
                retval = StateFactory.getInstance().createStateComponent(ReadingEmailState.class.getName());
            } else {
                retval = StateFactory.getInstance().createStateComponent(ClosingEmailState.class.getName());
            }
        } catch (MessagingException ex) {
            Logger.getLogger(SearchingEmailState.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retval;
    }
}
