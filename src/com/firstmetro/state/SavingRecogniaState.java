/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstmetro.state;

import com.firstmetro.emailreader.DataObject;
import com.firstmetro.model.Recognia;
import java.io.Console;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.datacontract.schemas._2004._07.mysqldb_firstmetroats.RecogniaTO;

/**
 *
 * @author Carlo
 */
public class SavingRecogniaState implements IState {

    @Override
    public IState next(DataObject dataObject) {
        IState retval = null;
        List<Recognia> recogniaList = dataObject.getRecognias();
        Logger.getLogger(SavingRecogniaState.class.getName()).log(Level.INFO, "Saving Recognia");
        for (Recognia recognia : recogniaList) {
            try {
                String result = insertRecognia(recognia.getSubject(), recognia.getMessage(), recognia.getDate(), recognia.getKeyword(), recognia.getStatus());
                Logger.getLogger(SavingRecogniaState.class.getName()).log(Level.INFO, result);
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(SavingRecogniaState.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        dataObject.getRecognias().clear();
        retval = StateFactory.getInstance().createStateComponent(ClosingEmailState.class.getName());
        return retval;
    }

    private static String insertRecognia(java.lang.String subject, java.lang.String message, java.lang.String date, java.lang.String keyword, java.lang.String status) {
        org.tempuri.StockService service = new org.tempuri.StockService();
        org.tempuri.IStockService port = service.getBasicHttpBindingIStockService();
        return port.insertRecognia(subject, message, date, keyword, status);
    }
}
