/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstmetro.state;

import com.firstmetro.emailreader.DataObject;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Vector;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlo
 */
public class StateManager implements Runnable, PropertyChangeListener {

    Vector<ScheduledFuture<?>> ScheduleHandlers;
    ScheduledExecutorService FScheduler;
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public StateManager(Vector<ScheduledFuture<?>> scheduleHandlers, ScheduledExecutorService fScheduler) {
        this.ScheduleHandlers = scheduleHandlers;
        this.FScheduler = fScheduler;
    }

    public void register(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }
    /**
     * Cancel Existing Handlers. Delete all the handlers in Vector. Re run the
     * Automated News Mailer.
     */
    @Override
    public void run() {
        Logger.getLogger(StateManager.class.getName()).log(Level.SEVERE, "State Manager Started");
        IState state = StateFactory.getInstance().createStateComponent(InitializeState.class.getName());
//        IState state = StateFactory.getInstance().createStateComponent(SendJsonState.class.getName());
        
        Logger.getLogger(StateManager.class.getName()).log(Level.SEVERE, state.toString());
        DataObject dataObject = new DataObject();

        if (FScheduler != null) {
            dataObject.setFScheduler(FScheduler);
            dataObject.setScheduleHandlers(ScheduleHandlers);
            while (state != null) {
                state = state.next(dataObject);
            }
        } else {
            Logger.getLogger(StateManager.class.getName()).log(Level.SEVERE, "fscheduler is null!");
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent pce) {
    }
}
