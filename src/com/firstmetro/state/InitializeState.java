/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstmetro.state;

import com.firstmetro.emailreader.DataObject;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlo
 */
public class InitializeState implements IState {

    @Override
    public IState next(DataObject dataObject) {
        IState retval = null;
        Logger.getLogger(InitializeState.class.getName()).log(Level.INFO, "Initialize called!");
        for (ScheduledFuture<?> scheduledFuture : dataObject.getScheduleHandlers()) {
            scheduledFuture.cancel(true);
        }
        dataObject.getScheduleHandlers().removeAllElements();
        retval = StateFactory.getInstance().createStateComponent(SettingEmailState.class.getName());
        return retval;
    }
}
