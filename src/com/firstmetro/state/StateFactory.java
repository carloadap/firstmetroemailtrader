/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstmetro.state;

import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlo
 */
public class StateFactory {

    private static StateFactory instance;
    @SuppressWarnings("UseOfObsoleteCollectionType")
    private Hashtable<String, IState> states = new Hashtable<>();

    public static StateFactory getInstance() {
        if (instance == null) {
            instance = new StateFactory();
        }
        return instance;
    }

    public IState createStateComponent(String name) {
        IState stateComponent = states.get(name);
        try {
            if (stateComponent == null) {
                Class<?> classObj = Class.forName(name);
                stateComponent = (IState) classObj.newInstance();
                states.put(name, stateComponent);
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(StateFactory.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
        return stateComponent;
    }
}
