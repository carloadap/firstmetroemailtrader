/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstmetro.state;

import com.firstmetro.emailreader.AppProperties;
import com.firstmetro.emailreader.DataObject;
import com.firstmetro.model.Recognia;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Carlo
 */
public class ReadingEmailState implements IState {

    @Override
    public IState next(DataObject dataObject) {
        IState retval = null;
        AppProperties appProperties = AppProperties.getInstance();
        String trigger = appProperties.getProperty("trigger");
        for (int i = 0; i < dataObject.getMessages().length; i++) {

            Message message = dataObject.getMessages()[i];
            String content1 = getContent2(message);
            //            if (content1.toLowerCase().contains(trigger.toLowerCase())) {

            Document doc = Jsoup.parse(content1);
            Elements trs = doc.select("tr"); //select all "tr" elements from document
            for (Element tr : trs) {
                //Getting the class string form tr element
                //System.out.println("The file class is: " + tr.attr("class") 
                //getting the filename string that holds inside td element
                // + " The filamee is: "  + tr.select("td").text());

                System.out.println(tr.select("td").text());
            }
            String trades = parseRecogniaEmail(content1);
            if (trades != null) {

                try {
                    String[] rows = trades.split(";");
                    for (String row : rows) {
                        System.out.println(row);
                        try {
                            //String[] columns = row.split(",");
                            Recognia recognia = new Recognia();
                            recognia.setSubject(message.getSubject());
                            recognia.setMessage(row);
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String myDateString = simpleDateFormat.format(message.getReceivedDate());
                            recognia.setDate(myDateString);
                            recognia.setKeyword(row);
                            recognia.setStatus("Open");
                            dataObject.getRecognias().add(recognia);
                        } catch (MessagingException ex) {
                            Logger.getLogger(ReadingEmailState.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }

                    message.setFlag(Flags.Flag.DELETED, true);
                } catch (MessagingException ex) {
                    Logger.getLogger(ReadingEmailState.class.getName()).log(Level.SEVERE, null, ex);
                }
                //          }
                retval = StateFactory.getInstance().createStateComponent(SavingRecogniaState.class.getName());

            } else {
                retval = StateFactory.getInstance().createStateComponent(ClosingEmailState.class.getName());

            }

        }
        return retval;
    }

    private String getContent2(Message message) {
        String html = null;
        try {
            Object content = message.getContent();
            if (content instanceof Multipart) {
                Multipart mp = (Multipart) content;
                for (int x = 0; x < mp.getCount(); x++) {
                    BodyPart bp = mp.getBodyPart(x);
                    if (Pattern
                            .compile(Pattern.quote("text/html"),
                            Pattern.CASE_INSENSITIVE)
                            .matcher(bp.getContentType()).find()) {
                        // found html part
                        html = (String) bp.getContent();
                    } else {
                        // some other bodypart...
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ReadingEmailState.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(ReadingEmailState.class.getName()).log(Level.SEVERE, null, ex);
        }
        return html;
    }

    private String parseRecogniaEmail(String html) {
        String values="";
        Document doc = Jsoup.parse(html, "UTF-8");
        Elements trs = doc.select("tr"); //select all "tr" elements from document
        int length = 0;
        String td;
        String[] columns;
        for (Element tr : trs) {
            td = tr.select("td").text();
            if ((td.endsWith("Bearish") || td.endsWith("Bullish")) && (!td.startsWith("Symbol"))) {
                columns = td.split(" ");
                length = columns.length;
                values += String.format("%s,%s %s;", columns[0],columns[length-2],columns[length-1]);
//                System.out.println(String.format("%s,%s %s", columns[0], columns[length - 2], columns[length - 1]));
            }
        }
        return values;
    }

    private String getTrades(String html) {
        String trades = new Scanner(html).useDelimiter("\\Z").next();
        String values = null;
        String lines = null;
        Pattern pattern = Pattern.compile("<span[^>]*>([^<]*)</span>", Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
        Matcher m = pattern.matcher(trades);
        boolean trigger = false;
        while (m.find()) {
            lines = m.group(1).replace("&amp;", "&");
            if (lines.contains("nbsp;")) {
                trigger = false;
            }
            if (trigger) {
//                System.out.println(m.group(1));
                if (lines.isEmpty()) {
                    if (values != null) {
                        values.substring(0, values.length() - 1);
                        values += ";\n";
                        System.out.println(values);
                    }
                } else {
                    if (values == null) {
                        values = String.format("%s,", lines);
                    } else {
                        if (m.group(1).contains("Term")) {
                            values += String.format("%s", lines);
                        } else {
                            values += String.format("%s,", lines);
                        }
                    }
                    System.out.println(values);
                }
            }

            if (lines.contains("Opportunity Type")) {
                trigger = true;
            }
        }
        return values;
    }

    public String getContent(Message msg) {
        String content = null;
        try {
            Multipart mp = (Multipart) msg.getContent();
            BodyPart part;
            String disposition;
            int count = mp.getCount();
            for (int i = 0; i < count - 1; i++) {
//                dumpPart(mp.getBodyPart(i));
                part = mp.getBodyPart(i);
                disposition = part.getDisposition();
                if (disposition != null && disposition.equals(Part.INLINE)) {
                    content = part.getContent().toString();
                } else {
                    content = part.getContent().toString();
                }
            }
        } catch (Exception ex) {
            System.out.println("Exception arise at get Content");
            ex.printStackTrace();
        }
        return content;
    }
}
