/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstmetro.state;

import com.firstmetro.emailreader.DataObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;

/**
 *
 * @author Carlo
 */
public class ClosingEmailState implements IState {

    @Override
    public IState next(DataObject dataObject) {
        Logger.getLogger(ClosingEmailState.class.getName()).log(Level.INFO, "Closing Email");
        try {
            dataObject.getFolder().close(false);
            dataObject.getStore().close();
        } catch (MessagingException ex) {
            Logger.getLogger(ClosingEmailState.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
