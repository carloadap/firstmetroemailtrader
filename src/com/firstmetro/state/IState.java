/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstmetro.state;

import com.firstmetro.emailreader.DataObject;

/**
 *
 * @author Carlo
 */
public interface IState {
    public IState next(DataObject dataObject);
}

