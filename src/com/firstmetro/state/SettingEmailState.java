/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstmetro.state;

import com.firstmetro.emailreader.AppProperties;
import com.firstmetro.emailreader.DataObject;
import java.util.Arrays;
import java.util.Properties;
import javax.mail.Session;

/**
 *
 * @author Carlo
 */
public class SettingEmailState implements IState {

    @Override
    public IState next(DataObject dataObject) {
        IState retval = null;
        AppProperties appProperties = AppProperties.getInstance();
        appProperties.load();
        String host = appProperties.getProperty("host");
        String port = appProperties.getProperty("port");
        String userName = appProperties.getProperty("username");
        String password = appProperties.getProperty("password");
        String smtpHost = appProperties.getProperty("smtphost");
        String smtpServer = appProperties.getProperty("smtpserver");
//        EmailSearcher searcher = new EmailSearcher();
        String[] keyword = appProperties.getProperty("keyword").split(",");
        Properties properties = new Properties();
        properties.put("mail.imap.host", host);
        properties.put("mail.imap.port", port);
        properties.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.imap.socketFactory.fallback", "false");
        properties.setProperty("mail.imap.socketFactory.port", String.valueOf(port));
        Session session = Session.getDefaultInstance(properties);
        dataObject.setSession(session);
        dataObject.setUsername(userName);
        dataObject.setPassword(password);
        dataObject.getKeyword().addAll(Arrays.asList(keyword));
        dataObject.setSmtpHost(smtpHost);
        dataObject.setSmtpServer(smtpServer);
        retval = StateFactory.getInstance().createStateComponent(ConnectingToEmailState.class.getName());
        return retval;
    }
}
