/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstmetro.state;

import com.firstmetro.emailreader.DataObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Store;

/**
 *
 * @author Carlo
 */
public class ConnectingToEmailState implements IState {

    @Override
    public IState next(DataObject dataObject) {
        IState retval = null;
        try {
            Store store = dataObject.getSession().getStore("imap");
            store.connect(dataObject.getUsername(), dataObject.getPassword());
            Folder folderInbox = store.getFolder("INBOX");
            folderInbox.open(Folder.READ_WRITE);
            dataObject.setFolder(folderInbox);
            dataObject.setStore(store);
            retval = StateFactory.getInstance().createStateComponent(SearchingEmailState.class.getName());
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(ConnectingToEmailState.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(ConnectingToEmailState.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retval;
    }
}
