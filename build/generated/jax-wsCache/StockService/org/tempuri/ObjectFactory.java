
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import org.datacontract.schemas._2004._07.firstmetrowcfservice.QuotationScreenTO;
import org.datacontract.schemas._2004._07.mysqldb_firstmetroats.ArrayOfPriceWithTimeFrameTO;
import org.datacontract.schemas._2004._07.mysqldb_firstmetroats.ArrayOfRSITO;
import org.datacontract.schemas._2004._07.mysqldb_firstmetroats.ArrayOfRecogniaTO;
import org.datacontract.schemas._2004._07.mysqldb_firstmetroats.ArrayOfVolumeMATO;
import org.datacontract.schemas._2004._07.mysqldb_firstmetroats.RecogniaTO;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetVolumeMaListStockName_QNAME = new QName("http://tempuri.org/", "stockName");
    private final static QName _GetVolumeMaListTimeFrame_QNAME = new QName("http://tempuri.org/", "timeFrame");
    private final static QName _GetPriceWithTimeFrameListResponseGetPriceWithTimeFrameListResult_QNAME = new QName("http://tempuri.org/", "GetPriceWithTimeFrameListResult");
    private final static QName _InsertRecogniaResponseInsertRecogniaResult_QNAME = new QName("http://tempuri.org/", "InsertRecogniaResult");
    private final static QName _InsertRecogniaKeyword_QNAME = new QName("http://tempuri.org/", "keyword");
    private final static QName _InsertRecogniaSubject_QNAME = new QName("http://tempuri.org/", "subject");
    private final static QName _InsertRecogniaStatus_QNAME = new QName("http://tempuri.org/", "status");
    private final static QName _InsertRecogniaMessage_QNAME = new QName("http://tempuri.org/", "message");
    private final static QName _InsertRecogniaDate_QNAME = new QName("http://tempuri.org/", "date");
    private final static QName _GetTimeFrameResponseGetTimeFrameResult_QNAME = new QName("http://tempuri.org/", "GetTimeFrameResult");
    private final static QName _DoWorkResponseDoWorkResult_QNAME = new QName("http://tempuri.org/", "DoWorkResult");
    private final static QName _GetOpenRecogniaListResponseGetOpenRecogniaListResult_QNAME = new QName("http://tempuri.org/", "GetOpenRecogniaListResult");
    private final static QName _SaveToDBResponseSaveToDBResult_QNAME = new QName("http://tempuri.org/", "SaveToDBResult");
    private final static QName _GetRSIListResponseGetRSIListResult_QNAME = new QName("http://tempuri.org/", "GetRSIListResult");
    private final static QName _InsertRSIResponseInsertRSIResult_QNAME = new QName("http://tempuri.org/", "InsertRSIResult");
    private final static QName _GetVolumeMaListResponseGetVolumeMaListResult_QNAME = new QName("http://tempuri.org/", "GetVolumeMaListResult");
    private final static QName _InsertRecogniaTORTO_QNAME = new QName("http://tempuri.org/", "rTO");
    private final static QName _InsertPriceWithTimeFrameResponseInsertPriceWithTimeFrameResult_QNAME = new QName("http://tempuri.org/", "InsertPriceWithTimeFrameResult");
    private final static QName _DoWorkWork_QNAME = new QName("http://tempuri.org/", "work");
    private final static QName _InsertRecogniaTOResponseInsertRecogniaTOResult_QNAME = new QName("http://tempuri.org/", "InsertRecogniaTOResult");
    private final static QName _InsertVolumeMAResponseInsertVolumeMAResult_QNAME = new QName("http://tempuri.org/", "InsertVolumeMAResult");
    private final static QName _InsertQuotationScreenResponseInsertQuotationScreenResult_QNAME = new QName("http://tempuri.org/", "InsertQuotationScreenResult");
    private final static QName _InsertQuotationScreenQ_QNAME = new QName("http://tempuri.org/", "q");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetRSIListResponse }
     * 
     */
    public GetRSIListResponse createGetRSIListResponse() {
        return new GetRSIListResponse();
    }

    /**
     * Create an instance of {@link GetTimeFrame }
     * 
     */
    public GetTimeFrame createGetTimeFrame() {
        return new GetTimeFrame();
    }

    /**
     * Create an instance of {@link GetPriceWithTimeFrameList }
     * 
     */
    public GetPriceWithTimeFrameList createGetPriceWithTimeFrameList() {
        return new GetPriceWithTimeFrameList();
    }

    /**
     * Create an instance of {@link InsertRSI }
     * 
     */
    public InsertRSI createInsertRSI() {
        return new InsertRSI();
    }

    /**
     * Create an instance of {@link InsertPriceWithTimeFrameResponse }
     * 
     */
    public InsertPriceWithTimeFrameResponse createInsertPriceWithTimeFrameResponse() {
        return new InsertPriceWithTimeFrameResponse();
    }

    /**
     * Create an instance of {@link InsertRecognia }
     * 
     */
    public InsertRecognia createInsertRecognia() {
        return new InsertRecognia();
    }

    /**
     * Create an instance of {@link InsertVolumeMA }
     * 
     */
    public InsertVolumeMA createInsertVolumeMA() {
        return new InsertVolumeMA();
    }

    /**
     * Create an instance of {@link InsertRecogniaResponse }
     * 
     */
    public InsertRecogniaResponse createInsertRecogniaResponse() {
        return new InsertRecogniaResponse();
    }

    /**
     * Create an instance of {@link InsertVolumeMAResponse }
     * 
     */
    public InsertVolumeMAResponse createInsertVolumeMAResponse() {
        return new InsertVolumeMAResponse();
    }

    /**
     * Create an instance of {@link InsertPriceWithTimeFrame }
     * 
     */
    public InsertPriceWithTimeFrame createInsertPriceWithTimeFrame() {
        return new InsertPriceWithTimeFrame();
    }

    /**
     * Create an instance of {@link InsertRSIResponse }
     * 
     */
    public InsertRSIResponse createInsertRSIResponse() {
        return new InsertRSIResponse();
    }

    /**
     * Create an instance of {@link GetOpenRecogniaListResponse }
     * 
     */
    public GetOpenRecogniaListResponse createGetOpenRecogniaListResponse() {
        return new GetOpenRecogniaListResponse();
    }

    /**
     * Create an instance of {@link InsertRecogniaTOResponse }
     * 
     */
    public InsertRecogniaTOResponse createInsertRecogniaTOResponse() {
        return new InsertRecogniaTOResponse();
    }

    /**
     * Create an instance of {@link GetRSIList }
     * 
     */
    public GetRSIList createGetRSIList() {
        return new GetRSIList();
    }

    /**
     * Create an instance of {@link SaveToDB }
     * 
     */
    public SaveToDB createSaveToDB() {
        return new SaveToDB();
    }

    /**
     * Create an instance of {@link InsertQuotationScreenResponse }
     * 
     */
    public InsertQuotationScreenResponse createInsertQuotationScreenResponse() {
        return new InsertQuotationScreenResponse();
    }

    /**
     * Create an instance of {@link InsertQuotationScreen }
     * 
     */
    public InsertQuotationScreen createInsertQuotationScreen() {
        return new InsertQuotationScreen();
    }

    /**
     * Create an instance of {@link DoWork }
     * 
     */
    public DoWork createDoWork() {
        return new DoWork();
    }

    /**
     * Create an instance of {@link DoWorkResponse }
     * 
     */
    public DoWorkResponse createDoWorkResponse() {
        return new DoWorkResponse();
    }

    /**
     * Create an instance of {@link GetTimeFrameResponse }
     * 
     */
    public GetTimeFrameResponse createGetTimeFrameResponse() {
        return new GetTimeFrameResponse();
    }

    /**
     * Create an instance of {@link GetPriceWithTimeFrameListResponse }
     * 
     */
    public GetPriceWithTimeFrameListResponse createGetPriceWithTimeFrameListResponse() {
        return new GetPriceWithTimeFrameListResponse();
    }

    /**
     * Create an instance of {@link GetOpenRecogniaList }
     * 
     */
    public GetOpenRecogniaList createGetOpenRecogniaList() {
        return new GetOpenRecogniaList();
    }

    /**
     * Create an instance of {@link GetVolumeMaList }
     * 
     */
    public GetVolumeMaList createGetVolumeMaList() {
        return new GetVolumeMaList();
    }

    /**
     * Create an instance of {@link InsertRecogniaTO }
     * 
     */
    public InsertRecogniaTO createInsertRecogniaTO() {
        return new InsertRecogniaTO();
    }

    /**
     * Create an instance of {@link SaveToDBResponse }
     * 
     */
    public SaveToDBResponse createSaveToDBResponse() {
        return new SaveToDBResponse();
    }

    /**
     * Create an instance of {@link GetVolumeMaListResponse }
     * 
     */
    public GetVolumeMaListResponse createGetVolumeMaListResponse() {
        return new GetVolumeMaListResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "stockName", scope = GetVolumeMaList.class)
    public JAXBElement<String> createGetVolumeMaListStockName(String value) {
        return new JAXBElement<String>(_GetVolumeMaListStockName_QNAME, String.class, GetVolumeMaList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "timeFrame", scope = GetVolumeMaList.class)
    public JAXBElement<String> createGetVolumeMaListTimeFrame(String value) {
        return new JAXBElement<String>(_GetVolumeMaListTimeFrame_QNAME, String.class, GetVolumeMaList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPriceWithTimeFrameTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetPriceWithTimeFrameListResult", scope = GetPriceWithTimeFrameListResponse.class)
    public JAXBElement<ArrayOfPriceWithTimeFrameTO> createGetPriceWithTimeFrameListResponseGetPriceWithTimeFrameListResult(ArrayOfPriceWithTimeFrameTO value) {
        return new JAXBElement<ArrayOfPriceWithTimeFrameTO>(_GetPriceWithTimeFrameListResponseGetPriceWithTimeFrameListResult_QNAME, ArrayOfPriceWithTimeFrameTO.class, GetPriceWithTimeFrameListResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "InsertRecogniaResult", scope = InsertRecogniaResponse.class)
    public JAXBElement<String> createInsertRecogniaResponseInsertRecogniaResult(String value) {
        return new JAXBElement<String>(_InsertRecogniaResponseInsertRecogniaResult_QNAME, String.class, InsertRecogniaResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "keyword", scope = InsertRecognia.class)
    public JAXBElement<String> createInsertRecogniaKeyword(String value) {
        return new JAXBElement<String>(_InsertRecogniaKeyword_QNAME, String.class, InsertRecognia.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "subject", scope = InsertRecognia.class)
    public JAXBElement<String> createInsertRecogniaSubject(String value) {
        return new JAXBElement<String>(_InsertRecogniaSubject_QNAME, String.class, InsertRecognia.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "status", scope = InsertRecognia.class)
    public JAXBElement<String> createInsertRecogniaStatus(String value) {
        return new JAXBElement<String>(_InsertRecogniaStatus_QNAME, String.class, InsertRecognia.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "message", scope = InsertRecognia.class)
    public JAXBElement<String> createInsertRecogniaMessage(String value) {
        return new JAXBElement<String>(_InsertRecogniaMessage_QNAME, String.class, InsertRecognia.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "date", scope = InsertRecognia.class)
    public JAXBElement<String> createInsertRecogniaDate(String value) {
        return new JAXBElement<String>(_InsertRecogniaDate_QNAME, String.class, InsertRecognia.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPriceWithTimeFrameTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetTimeFrameResult", scope = GetTimeFrameResponse.class)
    public JAXBElement<ArrayOfPriceWithTimeFrameTO> createGetTimeFrameResponseGetTimeFrameResult(ArrayOfPriceWithTimeFrameTO value) {
        return new JAXBElement<ArrayOfPriceWithTimeFrameTO>(_GetTimeFrameResponseGetTimeFrameResult_QNAME, ArrayOfPriceWithTimeFrameTO.class, GetTimeFrameResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "stockName", scope = InsertVolumeMA.class)
    public JAXBElement<String> createInsertVolumeMAStockName(String value) {
        return new JAXBElement<String>(_GetVolumeMaListStockName_QNAME, String.class, InsertVolumeMA.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "timeFrame", scope = InsertVolumeMA.class)
    public JAXBElement<String> createInsertVolumeMATimeFrame(String value) {
        return new JAXBElement<String>(_GetVolumeMaListTimeFrame_QNAME, String.class, InsertVolumeMA.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "DoWorkResult", scope = DoWorkResponse.class)
    public JAXBElement<String> createDoWorkResponseDoWorkResult(String value) {
        return new JAXBElement<String>(_DoWorkResponseDoWorkResult_QNAME, String.class, DoWorkResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "stockName", scope = SaveToDB.class)
    public JAXBElement<String> createSaveToDBStockName(String value) {
        return new JAXBElement<String>(_GetVolumeMaListStockName_QNAME, String.class, SaveToDB.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRecogniaTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetOpenRecogniaListResult", scope = GetOpenRecogniaListResponse.class)
    public JAXBElement<ArrayOfRecogniaTO> createGetOpenRecogniaListResponseGetOpenRecogniaListResult(ArrayOfRecogniaTO value) {
        return new JAXBElement<ArrayOfRecogniaTO>(_GetOpenRecogniaListResponseGetOpenRecogniaListResult_QNAME, ArrayOfRecogniaTO.class, GetOpenRecogniaListResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "SaveToDBResult", scope = SaveToDBResponse.class)
    public JAXBElement<String> createSaveToDBResponseSaveToDBResult(String value) {
        return new JAXBElement<String>(_SaveToDBResponseSaveToDBResult_QNAME, String.class, SaveToDBResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRSITO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetRSIListResult", scope = GetRSIListResponse.class)
    public JAXBElement<ArrayOfRSITO> createGetRSIListResponseGetRSIListResult(ArrayOfRSITO value) {
        return new JAXBElement<ArrayOfRSITO>(_GetRSIListResponseGetRSIListResult_QNAME, ArrayOfRSITO.class, GetRSIListResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "InsertRSIResult", scope = InsertRSIResponse.class)
    public JAXBElement<String> createInsertRSIResponseInsertRSIResult(String value) {
        return new JAXBElement<String>(_InsertRSIResponseInsertRSIResult_QNAME, String.class, InsertRSIResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfVolumeMATO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetVolumeMaListResult", scope = GetVolumeMaListResponse.class)
    public JAXBElement<ArrayOfVolumeMATO> createGetVolumeMaListResponseGetVolumeMaListResult(ArrayOfVolumeMATO value) {
        return new JAXBElement<ArrayOfVolumeMATO>(_GetVolumeMaListResponseGetVolumeMaListResult_QNAME, ArrayOfVolumeMATO.class, GetVolumeMaListResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecogniaTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "rTO", scope = InsertRecogniaTO.class)
    public JAXBElement<RecogniaTO> createInsertRecogniaTORTO(RecogniaTO value) {
        return new JAXBElement<RecogniaTO>(_InsertRecogniaTORTO_QNAME, RecogniaTO.class, InsertRecogniaTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "stockName", scope = InsertRSI.class)
    public JAXBElement<String> createInsertRSIStockName(String value) {
        return new JAXBElement<String>(_GetVolumeMaListStockName_QNAME, String.class, InsertRSI.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "timeFrame", scope = InsertRSI.class)
    public JAXBElement<String> createInsertRSITimeFrame(String value) {
        return new JAXBElement<String>(_GetVolumeMaListTimeFrame_QNAME, String.class, InsertRSI.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "InsertPriceWithTimeFrameResult", scope = InsertPriceWithTimeFrameResponse.class)
    public JAXBElement<String> createInsertPriceWithTimeFrameResponseInsertPriceWithTimeFrameResult(String value) {
        return new JAXBElement<String>(_InsertPriceWithTimeFrameResponseInsertPriceWithTimeFrameResult_QNAME, String.class, InsertPriceWithTimeFrameResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "work", scope = DoWork.class)
    public JAXBElement<String> createDoWorkWork(String value) {
        return new JAXBElement<String>(_DoWorkWork_QNAME, String.class, DoWork.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "InsertRecogniaTOResult", scope = InsertRecogniaTOResponse.class)
    public JAXBElement<String> createInsertRecogniaTOResponseInsertRecogniaTOResult(String value) {
        return new JAXBElement<String>(_InsertRecogniaTOResponseInsertRecogniaTOResult_QNAME, String.class, InsertRecogniaTOResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "stockName", scope = GetRSIList.class)
    public JAXBElement<String> createGetRSIListStockName(String value) {
        return new JAXBElement<String>(_GetVolumeMaListStockName_QNAME, String.class, GetRSIList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "timeFrame", scope = GetRSIList.class)
    public JAXBElement<String> createGetRSIListTimeFrame(String value) {
        return new JAXBElement<String>(_GetVolumeMaListTimeFrame_QNAME, String.class, GetRSIList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "InsertVolumeMAResult", scope = InsertVolumeMAResponse.class)
    public JAXBElement<String> createInsertVolumeMAResponseInsertVolumeMAResult(String value) {
        return new JAXBElement<String>(_InsertVolumeMAResponseInsertVolumeMAResult_QNAME, String.class, InsertVolumeMAResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "stockName", scope = GetTimeFrame.class)
    public JAXBElement<String> createGetTimeFrameStockName(String value) {
        return new JAXBElement<String>(_GetVolumeMaListStockName_QNAME, String.class, GetTimeFrame.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "timeFrame", scope = GetTimeFrame.class)
    public JAXBElement<String> createGetTimeFrameTimeFrame(String value) {
        return new JAXBElement<String>(_GetVolumeMaListTimeFrame_QNAME, String.class, GetTimeFrame.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "InsertQuotationScreenResult", scope = InsertQuotationScreenResponse.class)
    public JAXBElement<String> createInsertQuotationScreenResponseInsertQuotationScreenResult(String value) {
        return new JAXBElement<String>(_InsertQuotationScreenResponseInsertQuotationScreenResult_QNAME, String.class, InsertQuotationScreenResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "stockName", scope = InsertPriceWithTimeFrame.class)
    public JAXBElement<String> createInsertPriceWithTimeFrameStockName(String value) {
        return new JAXBElement<String>(_GetVolumeMaListStockName_QNAME, String.class, InsertPriceWithTimeFrame.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "timeFrame", scope = InsertPriceWithTimeFrame.class)
    public JAXBElement<String> createInsertPriceWithTimeFrameTimeFrame(String value) {
        return new JAXBElement<String>(_GetVolumeMaListTimeFrame_QNAME, String.class, InsertPriceWithTimeFrame.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "stockName", scope = GetPriceWithTimeFrameList.class)
    public JAXBElement<String> createGetPriceWithTimeFrameListStockName(String value) {
        return new JAXBElement<String>(_GetVolumeMaListStockName_QNAME, String.class, GetPriceWithTimeFrameList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "timeFrame", scope = GetPriceWithTimeFrameList.class)
    public JAXBElement<String> createGetPriceWithTimeFrameListTimeFrame(String value) {
        return new JAXBElement<String>(_GetVolumeMaListTimeFrame_QNAME, String.class, GetPriceWithTimeFrameList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuotationScreenTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "q", scope = InsertQuotationScreen.class)
    public JAXBElement<QuotationScreenTO> createInsertQuotationScreenQ(QuotationScreenTO value) {
        return new JAXBElement<QuotationScreenTO>(_InsertQuotationScreenQ_QNAME, QuotationScreenTO.class, InsertQuotationScreen.class, value);
    }

}
