
package org.datacontract.schemas._2004._07.mysqldb_firstmetroats;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.mysqldb_firstmetroats package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfVolumeMATO_QNAME = new QName("http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", "ArrayOfVolumeMATO");
    private final static QName _ArrayOfRecogniaTO_QNAME = new QName("http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", "ArrayOfRecogniaTO");
    private final static QName _RSITO_QNAME = new QName("http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", "RSITO");
    private final static QName _ArrayOfRSITO_QNAME = new QName("http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", "ArrayOfRSITO");
    private final static QName _ArrayOfPriceWithTimeFrameTO_QNAME = new QName("http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", "ArrayOfPriceWithTimeFrameTO");
    private final static QName _RecogniaTO_QNAME = new QName("http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", "RecogniaTO");
    private final static QName _VolumeMATO_QNAME = new QName("http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", "VolumeMATO");
    private final static QName _PriceWithTimeFrameTO_QNAME = new QName("http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", "PriceWithTimeFrameTO");
    private final static QName _PriceWithTimeFrameTOStockName_QNAME = new QName("http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", "StockName");
    private final static QName _PriceWithTimeFrameTOTimeFrame_QNAME = new QName("http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", "TimeFrame");
    private final static QName _RecogniaTOStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", "Status");
    private final static QName _RecogniaTOKeyword_QNAME = new QName("http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", "Keyword");
    private final static QName _RecogniaTOSubject_QNAME = new QName("http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", "Subject");
    private final static QName _RecogniaTOMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", "Message");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.mysqldb_firstmetroats
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfRSITO }
     * 
     */
    public ArrayOfRSITO createArrayOfRSITO() {
        return new ArrayOfRSITO();
    }

    /**
     * Create an instance of {@link ArrayOfRecogniaTO }
     * 
     */
    public ArrayOfRecogniaTO createArrayOfRecogniaTO() {
        return new ArrayOfRecogniaTO();
    }

    /**
     * Create an instance of {@link ArrayOfPriceWithTimeFrameTO }
     * 
     */
    public ArrayOfPriceWithTimeFrameTO createArrayOfPriceWithTimeFrameTO() {
        return new ArrayOfPriceWithTimeFrameTO();
    }

    /**
     * Create an instance of {@link RecogniaTO }
     * 
     */
    public RecogniaTO createRecogniaTO() {
        return new RecogniaTO();
    }

    /**
     * Create an instance of {@link ArrayOfVolumeMATO }
     * 
     */
    public ArrayOfVolumeMATO createArrayOfVolumeMATO() {
        return new ArrayOfVolumeMATO();
    }

    /**
     * Create an instance of {@link RSITO }
     * 
     */
    public RSITO createRSITO() {
        return new RSITO();
    }

    /**
     * Create an instance of {@link VolumeMATO }
     * 
     */
    public VolumeMATO createVolumeMATO() {
        return new VolumeMATO();
    }

    /**
     * Create an instance of {@link PriceWithTimeFrameTO }
     * 
     */
    public PriceWithTimeFrameTO createPriceWithTimeFrameTO() {
        return new PriceWithTimeFrameTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfVolumeMATO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "ArrayOfVolumeMATO")
    public JAXBElement<ArrayOfVolumeMATO> createArrayOfVolumeMATO(ArrayOfVolumeMATO value) {
        return new JAXBElement<ArrayOfVolumeMATO>(_ArrayOfVolumeMATO_QNAME, ArrayOfVolumeMATO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRecogniaTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "ArrayOfRecogniaTO")
    public JAXBElement<ArrayOfRecogniaTO> createArrayOfRecogniaTO(ArrayOfRecogniaTO value) {
        return new JAXBElement<ArrayOfRecogniaTO>(_ArrayOfRecogniaTO_QNAME, ArrayOfRecogniaTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RSITO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "RSITO")
    public JAXBElement<RSITO> createRSITO(RSITO value) {
        return new JAXBElement<RSITO>(_RSITO_QNAME, RSITO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRSITO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "ArrayOfRSITO")
    public JAXBElement<ArrayOfRSITO> createArrayOfRSITO(ArrayOfRSITO value) {
        return new JAXBElement<ArrayOfRSITO>(_ArrayOfRSITO_QNAME, ArrayOfRSITO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPriceWithTimeFrameTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "ArrayOfPriceWithTimeFrameTO")
    public JAXBElement<ArrayOfPriceWithTimeFrameTO> createArrayOfPriceWithTimeFrameTO(ArrayOfPriceWithTimeFrameTO value) {
        return new JAXBElement<ArrayOfPriceWithTimeFrameTO>(_ArrayOfPriceWithTimeFrameTO_QNAME, ArrayOfPriceWithTimeFrameTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecogniaTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "RecogniaTO")
    public JAXBElement<RecogniaTO> createRecogniaTO(RecogniaTO value) {
        return new JAXBElement<RecogniaTO>(_RecogniaTO_QNAME, RecogniaTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VolumeMATO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "VolumeMATO")
    public JAXBElement<VolumeMATO> createVolumeMATO(VolumeMATO value) {
        return new JAXBElement<VolumeMATO>(_VolumeMATO_QNAME, VolumeMATO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PriceWithTimeFrameTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "PriceWithTimeFrameTO")
    public JAXBElement<PriceWithTimeFrameTO> createPriceWithTimeFrameTO(PriceWithTimeFrameTO value) {
        return new JAXBElement<PriceWithTimeFrameTO>(_PriceWithTimeFrameTO_QNAME, PriceWithTimeFrameTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "StockName", scope = PriceWithTimeFrameTO.class)
    public JAXBElement<String> createPriceWithTimeFrameTOStockName(String value) {
        return new JAXBElement<String>(_PriceWithTimeFrameTOStockName_QNAME, String.class, PriceWithTimeFrameTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "TimeFrame", scope = PriceWithTimeFrameTO.class)
    public JAXBElement<String> createPriceWithTimeFrameTOTimeFrame(String value) {
        return new JAXBElement<String>(_PriceWithTimeFrameTOTimeFrame_QNAME, String.class, PriceWithTimeFrameTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "StockName", scope = RSITO.class)
    public JAXBElement<String> createRSITOStockName(String value) {
        return new JAXBElement<String>(_PriceWithTimeFrameTOStockName_QNAME, String.class, RSITO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "TimeFrame", scope = RSITO.class)
    public JAXBElement<String> createRSITOTimeFrame(String value) {
        return new JAXBElement<String>(_PriceWithTimeFrameTOTimeFrame_QNAME, String.class, RSITO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "Status", scope = RecogniaTO.class)
    public JAXBElement<String> createRecogniaTOStatus(String value) {
        return new JAXBElement<String>(_RecogniaTOStatus_QNAME, String.class, RecogniaTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "Keyword", scope = RecogniaTO.class)
    public JAXBElement<String> createRecogniaTOKeyword(String value) {
        return new JAXBElement<String>(_RecogniaTOKeyword_QNAME, String.class, RecogniaTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "Subject", scope = RecogniaTO.class)
    public JAXBElement<String> createRecogniaTOSubject(String value) {
        return new JAXBElement<String>(_RecogniaTOSubject_QNAME, String.class, RecogniaTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "Message", scope = RecogniaTO.class)
    public JAXBElement<String> createRecogniaTOMessage(String value) {
        return new JAXBElement<String>(_RecogniaTOMessage_QNAME, String.class, RecogniaTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "StockName", scope = VolumeMATO.class)
    public JAXBElement<String> createVolumeMATOStockName(String value) {
        return new JAXBElement<String>(_PriceWithTimeFrameTOStockName_QNAME, String.class, VolumeMATO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model", name = "TimeFrame", scope = VolumeMATO.class)
    public JAXBElement<String> createVolumeMATOTimeFrame(String value) {
        return new JAXBElement<String>(_PriceWithTimeFrameTOTimeFrame_QNAME, String.class, VolumeMATO.class, value);
    }

}
