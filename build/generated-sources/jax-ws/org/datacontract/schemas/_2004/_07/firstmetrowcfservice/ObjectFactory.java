
package org.datacontract.schemas._2004._07.firstmetrowcfservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.firstmetrowcfservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _QuotationScreenTO_QNAME = new QName("http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", "QuotationScreenTO");
    private final static QName _QuotationScreenTOStockName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", "StockName");
    private final static QName _QuotationScreenTOAveragePrice_QNAME = new QName("http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", "AveragePrice");
    private final static QName _QuotationScreenTOTimeSent_QNAME = new QName("http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", "TimeSent");
    private final static QName _QuotationScreenTOExpiry_QNAME = new QName("http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", "Expiry");
    private final static QName _QuotationScreenTOTrader_QNAME = new QName("http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", "Trader");
    private final static QName _QuotationScreenTOStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", "Status");
    private final static QName _QuotationScreenTOAccountId_QNAME = new QName("http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", "AccountId");
    private final static QName _QuotationScreenTOType_QNAME = new QName("http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", "Type");
    private final static QName _QuotationScreenTOSide_QNAME = new QName("http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", "Side");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.firstmetrowcfservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link QuotationScreenTO }
     * 
     */
    public QuotationScreenTO createQuotationScreenTO() {
        return new QuotationScreenTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuotationScreenTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", name = "QuotationScreenTO")
    public JAXBElement<QuotationScreenTO> createQuotationScreenTO(QuotationScreenTO value) {
        return new JAXBElement<QuotationScreenTO>(_QuotationScreenTO_QNAME, QuotationScreenTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", name = "StockName", scope = QuotationScreenTO.class)
    public JAXBElement<String> createQuotationScreenTOStockName(String value) {
        return new JAXBElement<String>(_QuotationScreenTOStockName_QNAME, String.class, QuotationScreenTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", name = "AveragePrice", scope = QuotationScreenTO.class)
    public JAXBElement<String> createQuotationScreenTOAveragePrice(String value) {
        return new JAXBElement<String>(_QuotationScreenTOAveragePrice_QNAME, String.class, QuotationScreenTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", name = "TimeSent", scope = QuotationScreenTO.class)
    public JAXBElement<String> createQuotationScreenTOTimeSent(String value) {
        return new JAXBElement<String>(_QuotationScreenTOTimeSent_QNAME, String.class, QuotationScreenTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", name = "Expiry", scope = QuotationScreenTO.class)
    public JAXBElement<String> createQuotationScreenTOExpiry(String value) {
        return new JAXBElement<String>(_QuotationScreenTOExpiry_QNAME, String.class, QuotationScreenTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", name = "Trader", scope = QuotationScreenTO.class)
    public JAXBElement<String> createQuotationScreenTOTrader(String value) {
        return new JAXBElement<String>(_QuotationScreenTOTrader_QNAME, String.class, QuotationScreenTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", name = "Status", scope = QuotationScreenTO.class)
    public JAXBElement<String> createQuotationScreenTOStatus(String value) {
        return new JAXBElement<String>(_QuotationScreenTOStatus_QNAME, String.class, QuotationScreenTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", name = "AccountId", scope = QuotationScreenTO.class)
    public JAXBElement<String> createQuotationScreenTOAccountId(String value) {
        return new JAXBElement<String>(_QuotationScreenTOAccountId_QNAME, String.class, QuotationScreenTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", name = "Type", scope = QuotationScreenTO.class)
    public JAXBElement<String> createQuotationScreenTOType(String value) {
        return new JAXBElement<String>(_QuotationScreenTOType_QNAME, String.class, QuotationScreenTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model", name = "Side", scope = QuotationScreenTO.class)
    public JAXBElement<String> createQuotationScreenTOSide(String value) {
        return new JAXBElement<String>(_QuotationScreenTOSide_QNAME, String.class, QuotationScreenTO.class, value);
    }

}
