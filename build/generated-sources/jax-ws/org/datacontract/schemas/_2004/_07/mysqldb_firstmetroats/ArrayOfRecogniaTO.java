
package org.datacontract.schemas._2004._07.mysqldb_firstmetroats;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfRecogniaTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRecogniaTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecogniaTO" type="{http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model}RecogniaTO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRecogniaTO", propOrder = {
    "recogniaTO"
})
public class ArrayOfRecogniaTO {

    @XmlElement(name = "RecogniaTO", nillable = true)
    protected List<RecogniaTO> recogniaTO;

    /**
     * Gets the value of the recogniaTO property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the recogniaTO property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRecogniaTO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RecogniaTO }
     * 
     * 
     */
    public List<RecogniaTO> getRecogniaTO() {
        if (recogniaTO == null) {
            recogniaTO = new ArrayList<RecogniaTO>();
        }
        return this.recogniaTO;
    }

}
