
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.datacontract.schemas._2004._07.firstmetrowcfservice.QuotationScreenTO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="q" type="{http://schemas.datacontract.org/2004/07/FirstMetroWCFService.Model}QuotationScreenTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "q"
})
@XmlRootElement(name = "InsertQuotationScreen")
public class InsertQuotationScreen {

    @XmlElementRef(name = "q", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<QuotationScreenTO> q;

    /**
     * Gets the value of the q property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QuotationScreenTO }{@code >}
     *     
     */
    public JAXBElement<QuotationScreenTO> getQ() {
        return q;
    }

    /**
     * Sets the value of the q property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QuotationScreenTO }{@code >}
     *     
     */
    public void setQ(JAXBElement<QuotationScreenTO> value) {
        this.q = value;
    }

}
