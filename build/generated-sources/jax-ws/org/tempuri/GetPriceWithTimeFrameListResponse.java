
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.datacontract.schemas._2004._07.mysqldb_firstmetroats.ArrayOfPriceWithTimeFrameTO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetPriceWithTimeFrameListResult" type="{http://schemas.datacontract.org/2004/07/MySqlDb.FirstMetroATS.Model}ArrayOfPriceWithTimeFrameTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPriceWithTimeFrameListResult"
})
@XmlRootElement(name = "GetPriceWithTimeFrameListResponse")
public class GetPriceWithTimeFrameListResponse {

    @XmlElementRef(name = "GetPriceWithTimeFrameListResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfPriceWithTimeFrameTO> getPriceWithTimeFrameListResult;

    /**
     * Gets the value of the getPriceWithTimeFrameListResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPriceWithTimeFrameTO }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPriceWithTimeFrameTO> getGetPriceWithTimeFrameListResult() {
        return getPriceWithTimeFrameListResult;
    }

    /**
     * Sets the value of the getPriceWithTimeFrameListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPriceWithTimeFrameTO }{@code >}
     *     
     */
    public void setGetPriceWithTimeFrameListResult(JAXBElement<ArrayOfPriceWithTimeFrameTO> value) {
        this.getPriceWithTimeFrameListResult = value;
    }

}
