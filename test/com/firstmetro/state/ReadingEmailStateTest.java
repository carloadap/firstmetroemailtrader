/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstmetro.state;

import com.google.common.collect.Sets;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Carlo
 */
public class ReadingEmailStateTest {

    public ReadingEmailStateTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of next method, of class ReadingEmailState.
     */
    @Test
    public void testNext() {
    }

    /**
     * Test of getContent method, of class ReadingEmailState.
     */
    @Test
    public void testGetContent() {
//        String tableTag =
//                Pattern.compile(".*?<table.*?claroTable.*?>(.*?)</table>.*?", Pattern.DOTALL)
//                .matcher(html)
//                .replaceFirst("$1");
//
//        System.out.println(tableTag);
    }

    @Test
    public void testParseHtmlJsoup2() throws FileNotFoundException {
        String html = new Scanner(new File("test.txt")).useDelimiter("\\Z").next();
        Document doc = Jsoup.parse(html);
        Elements tds = doc.select("td"); //select all "tr" elements from document
        for (Element td : tds) {
            //Getting the class string form tr element
            //System.out.println("The file class is: " + tr.attr("class") 
            //getting the filename string that holds inside td element
            // + " The filamee is: "  + tr.select("td").text());
            //System.out.println(tr.attr("class"));
            System.out.println(td.select("span").text());
        }
    }

    @Test
    public void testParseHtmlJsoup() throws FileNotFoundException {
        String values = "";
        String html = new Scanner(new File("test.txt")).useDelimiter("\\Z").next();
        Document doc = Jsoup.parse(html, "UTF-8");
        Elements trs = doc.select("tr"); //select all "tr" elements from document
        int length=0;
        String td;
        String[] columns;
        for (Element tr : trs) {
            td = tr.select("td").text();
            if ((td.endsWith("Bearish") || td.endsWith("Bullish")) && (!td.startsWith("Symbol"))) {
                columns = td.split(" ");
                length = columns.length;
                values += String.format("%s,%s,%s %s;", td,columns[0],columns[length-2],columns[length-1]);
                //System.out.println(String.format("%s,%s %s", columns[0],columns[length-2],columns[length-1]));
            }
        }
        System.out.println(values);
    }

    @Test
    public void testParseHTML2() throws FileNotFoundException {
        String html = new Scanner(new File("test.txt")).useDelimiter("\\Z").next();
        //System.out.println(html);
        Pattern p = Pattern.compile(".*?<table.*?ecxMsoNormalTable.*?>(.*?)</table>.*?", Pattern.DOTALL);
        Pattern p2 = Pattern.compile("<table[^>]*>(.*)</table>");
        Pattern p3 = Pattern.compile("<span[^>]*>(.*)</span>", Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
        //<span[^>]*>([^<]*)</span>
        Pattern pattern = Pattern.compile("<span[^>]*>([^<]*)</span>");
        //String symbol, exchance, name, event, closeAtEvent, targetPriceRange, opportunityType;
        String values = null;
        Matcher m = pattern.matcher(html);
        boolean trigger = false;
        while (m.find()) {
            // System.out.println("Found value: " + m.group(0));
            System.out.println("Found value: " + m.group(1));
            //System.out.println("Found value: " + m.group(2));
        }

    }

    @Test
    public void testParseHTML() {
        try {
            String html = new Scanner(new File("test.txt")).useDelimiter("\\Z").next();
            //System.out.println(html);
            Pattern p = Pattern.compile(".*?<table.*?ecxMsoNormalTable.*?>(.*?)</table>.*?", Pattern.DOTALL);
            Pattern p2 = Pattern.compile("<table[^>]*>(.*)</table>");
            Pattern p3 = Pattern.compile("<td[^>]*>(.*)</td>");
            //<span[^>]*>([^<]*)</span>
            Pattern pattern = Pattern.compile("<td[^>]*>([^<]*)</td>", Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);

            //String symbol, exchance, name, event, closeAtEvent, targetPriceRange, opportunityType;
            String values = null;
            Matcher m = pattern.matcher(html);
            boolean trigger = false;
            while (m.find()) {
                if (m.group(1).contains("nbsp;")) {
                    trigger = false;
                }
                //System.out.println(m.group(0));
                //System.out.println(m.group(1));
                //System.out.println(m.group(1));
                if (trigger) {
                    if (m.group(1).isEmpty()) {
                        if (values != null) {
                            //values.substring(0, values.length() - 1);
                            values += ";\n";
                        }
                    } else {
                        System.out.println(m.group(1));
                        if (values == null) {
                            values = String.format("%s,", m.group(1));
                        } else {
                            if (m.group(1).contains("Term")) {
                                values += String.format("%s", m.group(1));
                            } else {
                                values += String.format("%s,", m.group(1));
                            }
                        }
                        System.out.println(values);
                    }
                }
                if (m.group(1).contains("Opportunity Type")) {
                    trigger = true;
                }



                //System.out.println(values);

                //if()
            }
            System.out.println(values);
            //System.out.println(table);

//            Matcher m2 = p2.matcher(table);
//            while (m2.find()) {
//                System.out.println(m2.group(0));
//                System.out.println(m2.group(1));
//                row = m2.group(1);
//            }
//            Matcher m3 = p3.matcher(table);
//            while (m3.find()) {
//                System.out.println(m3.group(0));
//                System.out.println(m3.group(1));
//                row = m3.group(1);
//            }

//            String[] rows = values.split(";");
//            for (String row : rows) {
//                String[] columns = row.split(",");
//                for (String string : columns) {
//                    //           System.out.println(string);
//                }
//            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadingEmailStateTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}